
# Test project

## Используемые технологии
[C++14](https://ru.wikipedia.org/wiki/C%2B%2B14)  
[Qt5](https://ru.wikipedia.org/wiki/Qt)

## Системные требования
Windows

## Список контрибьюторов
[@github/AlexandrDedckov](../../../../AlexandrDedckov)

## Description
Given the information in the joint timetable, write a program to produce two modified timetables, one
for Posh Bus Company and one for Grotty Bus Company, each satisfying the following requirements:

1. All entries in each timetable are in order of departure time.
2. Any service longer than an hour shall not be included.
3. Only efficient services shall be added to the timetable. A service is considered efficient compared to the other one:

	3.1 If it starts at the same time and reaches earlier, or
    
	3.2 If it starts later and reaches at the same time, or
    
	3.3 If it starts later and reaches earlier.
    
	3.4 If both companies offer a service having the same departure and arrival times then always choose

Posh Bus Company over Grotty Bus Company, since Grotty Bus Company busses are not as comfortable
as those of Posh Bus Company. 

