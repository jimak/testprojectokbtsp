#include "filedata.h"

FileData::FileData(QObject *parent)
    : Data(parent)
{
}

bool FileData::exec(const QString &fileName)
{
    if (!fileName.isEmpty())
        _fileName = fileName;

    if (_fileName.isEmpty())
    {
        qDebug() << "EMPTY PATH";
        return false;
    }

    _file.setFileName(_fileName);
    if (!_file.exists())
    {
        qDebug() << "NOT FILE IN DIRECTORY";
        return false;
    }

    return true;
}

QVector<ObjBusShp> FileData::getObjBus()
{
    QVector <ObjBusShp> items;
    if(_file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        qDebug() << "-------READ FILE-------";
        while(!_file.atEnd())
        {
            const auto &line = _file.readLine().trimmed();
            qDebug() << line;
            items.append(ObjBusShp::create(line));
        }

        _file.close();
        qDebug() << "-----------------------";
    }
    else
        qDebug() << endl << "DON'T OPEN FILE" << endl;

    return items;
}

void FileData::saveObjBus(QVector<ObjBusShp> objArray)
{
    _file.setFileName(QDir::currentPath() + "/BusSchedule.txt");
    if (_file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QStringList listGrotty;
        QTextStream outputStream(&_file);
        for (auto obj : objArray)
        {
            bool isGrotty = false;
            const auto &line = createRow(obj, isGrotty);

            if (isGrotty)
            {
                listGrotty.append(line);
                continue;
            }

            outputStream << line<< endl;
        }

        outputStream << endl;
        for (auto line : listGrotty)
            outputStream << line<< endl;

        _file.close();
    }
    else
        qDebug() << endl << "DON'T OPEN FILE" << endl;
}

QString FileData::createRow(ObjBusShp obj, bool &flag)
{
    QString outputString = QString("%1 %2 %3")
            .arg(obj->name())
            .arg(obj->departure().toString("hh:mm"))
            .arg(obj->arrival().toString("hh:mm"));

    if ("Grotty" == obj->name())
        flag = true;

    return outputString;
}
