#include "createbusschedule.h"

#include <QDebug>
#include <QTextStream>
#include <QProcess>

QTextStream in(stdin);

CreateBusSchedule::CreateBusSchedule(QObject *parent)
    : QObject(parent)
{
}

void CreateBusSchedule::exec()
{
    qDebug() << "Enter path file";
    const QString fileName = in.readLine();

    _fileData = new FileData(this);
    if (!_fileData->exec(fileName))
        return;

    _objBusArray = _fileData->getObjBus();

    sortedArray();
    filterArray();

    _fileData->saveObjBus(_objBusArray);
    openNewFile();
}

void CreateBusSchedule::sortedArray()
{
    auto liambda = [](ObjBusShp a, ObjBusShp b) -> bool
    {
        QTime aObjDeparture = a->departure();
        QTime bObjDeparture = b->departure();

        if (!aObjDeparture.isValid())
            return false;

        if (!bObjDeparture.isValid())
            return true;

        return (aObjDeparture < bObjDeparture);
    };

    qSort(_objBusArray.begin(), _objBusArray.end(), liambda);
}

void CreateBusSchedule::filterArray()
{
    auto tempArray = _objBusArray;
    for (auto obj : _objBusArray)
    {
        int counterTemp = 0;
        while (counterTemp < tempArray.count())
        {
            auto tempObj = tempArray[counterTemp];
            if ( (obj->compare(tempObj.data())) || (tempObj->badRoute()) )
                tempArray.remove(counterTemp);
            else
                ++counterTemp;
        }
    }

    _objBusArray = tempArray;
}

void CreateBusSchedule::openNewFile()
{
    QProcess::execute("cmd /c " + QDir::currentPath() + "/BusSchedule.txt");
}

void CreateBusSchedule::debugList()
{
    qDebug() << endl << "[COUNT]" << _objBusArray.count();
    qDebug() << "----------DEBUG FILE----------";
    for (auto obj : _objBusArray)
    {
        qDebug() << obj->name()
                 << obj->departure().toString("hh:mm")
                 << obj->arrival().toString("hh:mm");
    }
    qDebug() << "------------------------------";
}

