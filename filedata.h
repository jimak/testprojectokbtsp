#pragma once

#include "data.h"
#include <QDir>
#include <QFile>


class FileData : public Data
{
    Q_OBJECT
public:
    explicit FileData(QObject *parent = nullptr);
    ~FileData() = default;

    bool exec(const QString &fileName = QString());

    QVector <ObjBusShp> getObjBus() override;
    void saveObjBus(QVector <ObjBusShp> objArray) override;

private:
    QString createRow(ObjBusShp obj, bool &flag);

private:
    QFile _file;
    QString _fileName;
};
