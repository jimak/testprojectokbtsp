#pragma once

#include <QObject>
#include <QDateTime>


class ObjBus : public QObject
{
    Q_OBJECT
public:
    explicit ObjBus(const QString &line, QObject *parent = nullptr);
    ~ObjBus() = default;

    QString name() const;
    void setName(const QString &name);

    QTime departure() const;
    void setDeparture(const QTime &departure);

    QTime arrival() const;
    void setArrival(const QTime &arrival);

    bool badRoute();
    bool compare(ObjBus *obj);

private:
    QString _name;
    QTime _departure;
    QTime _arrival;
};

typedef QSharedPointer<ObjBus> ObjBusShp;
