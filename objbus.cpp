#include "objbus.h"
#include "definitions.h"

ObjBus::ObjBus(const QString &line, QObject *parent)
    : QObject(parent)
{
    QStringList items = line.split(" ");

    _name = items.value(NAME_IN_LIST);
    _departure = QTime::fromString(items.value(DEPARTURE_IN_LIST), "hh:mm");
    _arrival = QTime::fromString(items.value(ARRIVAL_IN_LIST), "hh:mm");
}

QString ObjBus::name() const
{
    return _name;
}

void ObjBus::setName(const QString &name)
{
    _name = name;
}

QTime ObjBus::departure() const
{
    return _departure;
}

void ObjBus::setDeparture(const QTime &departure)
{
    _departure = departure;
}

QTime ObjBus::arrival() const
{
    return _arrival;
}

void ObjBus::setArrival(const QTime &arrival)
{
    _arrival = arrival;
}

bool ObjBus::badRoute()
{
    auto timeRoute = _departure.secsTo(_arrival) / SECOND_IN_MINUTE;
    if (timeRoute > COUNT_MINUTES_IN_HOUR)
        return true;

    return false;
}

bool ObjBus::compare(ObjBus *obj)
{
    if (this == obj)
        return false;

    const auto objDeparture = obj->departure();
    const auto objArrival = obj->arrival();

    if ((this->_departure == objDeparture)
            && (this->arrival() == objArrival))
    {
        if (this->_name != obj->name())
        {
            if ("Grotty" == obj->name())
                return true;
            else
                return false;
        }
        else
            return false;
    }
    else if ((this->_departure >= objDeparture)
             && (this->_arrival <= objArrival))
        return true;
    else
        return false;

    return false;
}
