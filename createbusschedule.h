#pragma once

#include <QObject>
#include "filedata.h"

class CreateBusSchedule : public QObject
{
    Q_OBJECT
public:
    explicit CreateBusSchedule(QObject *parent = nullptr);
    ~CreateBusSchedule() = default;

    void exec();

private:
    void sortedArray();
    void filterArray();
    void openNewFile();

    void debugList();

private:
    FileData *_fileData;
    QVector <ObjBusShp> _objBusArray;
};
