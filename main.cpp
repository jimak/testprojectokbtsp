#include <QCoreApplication>
#include "createbusschedule.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    CreateBusSchedule busSchedule;
    busSchedule.exec();

    return a.exec();
}
