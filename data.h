#pragma once

#include <QObject>
#include <QDebug>

#include "objbus.h"

class Data : public QObject
{
    Q_OBJECT
public:
    explicit Data(QObject *parent = nullptr);

    virtual QVector<ObjBusShp> getObjBus() = 0;
    virtual void saveObjBus(QVector<ObjBusShp> inputArray) = 0;
};
